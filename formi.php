<?php
require_once "pdo.php";
session_start();
if ( isset($_POST['as_Sposti']) && isset($_POST['as_Sala']) && isset($_POST['as_Etunimi']) && isset($_POST['as_Sukunimi']) && isset($_POST['as_Puhelin']) && isset($_POST['as_Kieli'])) {
    $sql = "INSERT INTO rekisteri (as_Sposti, as_Sala, as_Etunimi, as_Sukunimi, as_Puhelin, as_Kieli)
        VALUES (:as_Sposti, :as_Sala, :as_Etunimi, :as_Sukunimi, :as_Puhelin, :as_Kieli)";
    $stmt = $conn->prepare($sql);
    $stmt->execute(array(
        ':as_Sposti' => $_POST['as_Sposti'],
        ':as_Sala' => $_POST['as_Sala'],
        ':as_Etunimi' => $_POST['as_Etunimi'],
        ':as_Sukunimi' => $_POST['as_Sukunimi'],
        ':as_Puhelin' => $_POST['as_Puhelin'],
        ':as_Kieli' => $_POST['as_Kieli']));
    $_SESSION['success'] = 'Record Added';
    header( 'Location: alku.php' ) ;
    return;
}
else;
    echo "wow";
?>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
<link rel="stylesheet" href="../CSS/formi.css">
<body>
    <div class="container">
        <form method="post"> 
            <div class="mb-3">
                <label for="Sposti" class="form-label">Sähköposti</label>
                <input name="as_Sposti" type="email" class="form-control" id="Sposti" required>
            </div>
            <div class="mb-3">
                <label for="Salasana" class="form-label">Salasana</label>
                <input name="as_Sala" type="password" class="form-control" id="Salasana" required>
            </div>
            <div class="row">
            <div class="col">
                <label for="etunimi" class="form-label">Etunimi</label>
                <input name="as_Etunimi" id="etunimi" type="text" class="form-control" placeholder="Etunimi" aria-label="First name" required>
            </div>
            <div class="col">
                <label for="sukunimi" class="form-label">Sukunimi</label>
                <input name="as_Sukunimi" id="sukunimi" type="text" class="form-control" placeholder="Sukunimi" aria-label="Last name" required>
            </div>
            </div>
            <div class="mb-3">
                <label for="puhnro" class="form-label">Puhelin numero</label>
                <input name="as_Puhelin" type="text" class="form-control" id="puhnro" required>
            </div>
            <div class="mb-3">
            <label for="kieli" class="form-label">Kieli</label>
            <select name="as_Kieli" class="form-select" id="kieli" aria-label="Default select example" required>
            <option value="">Valitse kieli</option>
            <option value="suomi">suomi</option>
            <option value="ruotsi">ruotsi</option>
            <option value="englanti">englanti</option>
            </select>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
        <a href="alku.php">Lista</a>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>
</body>
